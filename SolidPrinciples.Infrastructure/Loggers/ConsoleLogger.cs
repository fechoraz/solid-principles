﻿using SolidPrinciples.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace SolidPrinciples.Infrastructure
{
    /// <summary>
    /// Logging is delegated
    /// </summary>
    public class ConsoleLogger : ILogger
    {
        public void Log(string message)
        {
            Console.WriteLine(message);
        }
    }
}
