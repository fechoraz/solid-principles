﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SolidPrinciples.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace SolidPrinciples.Infrastructure
{
    /// <summary>
    /// Encoding Format
    /// </summary>
    public class JsonPolicySerializer : IPolicySerializer
    {
        public Policy GetPolicyFromJsonString(string policyJson)
        {
           return JsonConvert.DeserializeObject<Policy>(policyJson, new StringEnumConverter());
        }
    }
}
