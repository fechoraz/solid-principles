﻿using SolidPrinciples.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SolidPrinciples.Infrastructure
{
    /// <summary>
    /// Persistence
    /// </summary>
    public class FilePolicySource : IPolicySource
    {
        public string GetPolicyFromSource()
        {
            return File.ReadAllText("policy.json");
        }        
    }
}
