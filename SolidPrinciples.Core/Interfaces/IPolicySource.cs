﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolidPrinciples.Core
{
    public interface IPolicySource
    {
        string GetPolicyFromSource();
    }
}
