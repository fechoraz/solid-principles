﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SolidPrinciples.Core
{
    public class FloodPolicyRater : Rater
    {
        public FloodPolicyRater(ILogger logger) : base(logger) { }

        public override decimal Rate(Policy policy)
        {
            Logger.Log("Rating FLOOD policy...");
            Logger.Log("Validating policy.");

            if(policy.BondAmount == 0 || policy.Valuation == 0)
            {
                Logger.Log("Flood policy must specify Bond Amount and Validation.");
                return 0M;
            }
            if(policy.ElevationAboveSeaLevelFeet <= 0)
            {
                Logger.Log("Flood policy is not available for elevations at or below sea level.");
                return 0M;
            }
            if(policy.BondAmount < 0.8M * policy.Valuation)
            {
                Logger.Log("Insufficient bond amount.");
                return 0M;
            }
            decimal multiple = 1.0M;
            if(policy.ElevationAboveSeaLevelFeet < 100)
            {
                multiple = 2.0M;
            }
            else if(policy.ElevationAboveSeaLevelFeet < 500)
            {
                multiple = 1.5M;
            }
            else if (policy.ElevationAboveSeaLevelFeet < 1000)
            {
                multiple = 1.1M;
            }
           return policy.BondAmount * 0.05m * multiple;
        }
    }
}
