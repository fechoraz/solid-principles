﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SolidPrinciples.Core
{
    public class RaterFactory
    {
        private readonly ILogger _logger;        
        public RaterFactory(ILogger logger)
        {
            _logger = logger;

        }
        public Rater Create(Policy policy)
        {
            try
            {                
                var policyType = Type.GetType($"SolidPrinciples.Core.{policy.Type}PolicyRater");

                var rater = (Rater)Activator.CreateInstance(policyType,
                    new object[] { _logger });

                return (Rater)Activator.CreateInstance(policyType, 
                    new object[] { _logger });
            }
            catch
            {
                return new UnknownPolicyRater(_logger);
            }            
        }
    }
}
