﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SolidPrinciples.Core
{
    public abstract class Rater
    {
        protected ILogger Logger;

        public Rater(ILogger logger)
        {
            Logger = logger;          
        }

        public abstract decimal Rate(Policy policy);
    }
}
